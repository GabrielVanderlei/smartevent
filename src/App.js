import Amplify, { Auth } from 'aws-amplify';
import awsconfig from './aws-exports';
import { withAuthenticator } from 'aws-amplify-react-native';

Amplify.configure(awsconfig);

let App = () => (
    <h1>Ok</h1>
)

export default withAuthenticator(App)
