import { registerRootComponent } from 'expo';
import { StatusBar } from 'expo-status-bar';
import React, { useEffect, useState } from 'react';

import Amplify, { Auth } from 'aws-amplify';
import awsconfig from './aws-exports';
import { withAuthenticator } from 'aws-amplify-react-native';

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import HomeScreen from './screens/HomeScreen';
import EventLocateScreen from './screens/EventLocateScreen';
import EventCreateScreen from './screens/EventCreateScreen';
import EventViewScreen from './screens/EventViewScreen';
import SubscriberViewScreen from './screens/SubscriberViewScreen';
import SubscriberCreateScreen from './screens/SubscriberCreateScreen';

Amplify.configure(awsconfig);

function App() {
  const Stack = createNativeStackNavigator();

  return (
    <>
      <StatusBar style="auto" />
      <NavigationContainer>
        <Stack.Navigator initialRouteName="HomeScreen">
          <Stack.Screen name="HomeScreen" options={{ title: 'Eventos' }} component={HomeScreen} />
          <Stack.Screen name="EventCreateScreen" options={{ title: 'Novo evento' }} component={EventCreateScreen} />
          <Stack.Screen name="EventLocateScreen" options={{ title: 'Localizar evento' }} component={EventLocateScreen} />
          <Stack.Screen name="EventViewScreen" options={{ title: 'Informações' }} component={EventViewScreen} />
          <Stack.Screen name="SubscriberViewScreen" options={{ title: 'Informações' }} component={SubscriberViewScreen} />
          <Stack.Screen name="SubscriberCreateScreen" options={{ title: 'Informações' }} component={SubscriberCreateScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    </>
  );
}

export default withAuthenticator(App);