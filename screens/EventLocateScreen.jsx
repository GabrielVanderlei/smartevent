import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';

import { TextInput } from 'react-native-paper';
import { Button } from 'react-native-paper';

function EventLocateScreen({ navigation }) {
    const [text, setText] = React.useState('');
  
    return (
      <>
        <View styles={styles.container}>
          <View style={{padding: 40}}>
            <Text style={{
              fontSize: 40
            }}>
              Qual o identificador do evento que deseja entrar?
            </Text>
            
            <TextInput
              label="Identificador"
              value={text}
              style={{marginTop: 20}}
              onChangeText={text => setText(text)}
            />
  
            <Button style={{marginTop: 20}} icon="calendar-search" mode="contained" onPress={() => console.log('Pressed')}>
              Localizar
            </Button>
            <Button style={{marginTop: 20}} icon="calendar-search" mode="contained" onPress={() => navigation.navigate('EventCreateScreen')}>
              Novo evento
            </Button>
          </View>
        </View>
      </>
    );
  }

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center'
    },
  
    fab: {
      position: 'absolute',
      margin: 16,
      right: 0,
      bottom: 0,
    },
  });
  
  export default EventLocateScreen;