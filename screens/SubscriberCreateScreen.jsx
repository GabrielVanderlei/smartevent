import React, { useEffect, useState } from 'react';
import { ScrollView, StyleSheet, Text, View } from 'react-native';

import { TextInput } from 'react-native-paper';
import { Button } from 'react-native-paper';

function SubscriberCreateScreen({ route, navigation }) {
    const { sk } = route.params;

    const [formData, setFormData] = React.useState({
        name: '',
        lastName: '',
        identifier: '',
        history: '',
        type: '',
        cost: '',
        obs: '',
        role: ''
    });

    const createElement = async () => {
        let allElements = await fetch(`https://h8ojzpcs01.execute-api.us-east-2.amazonaws.com/subscriber?sk=${sk}`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(formData)
        }).then((data) => data.json());

        if(allElements.data){
            alert(JSON.stringify(allElements));
            navigation.navigate('SubscriberViewScreen', { sk })
        } else {
            alert(JSON.stringify(allElements));
            alert("Erro ao adicionar inscrição")
        }
    }
  
    return (
      <>
        <ScrollView>
          <View  style={styles.container}>
            <TextInput
              label="Nome"
              value={formData.name}
              style={{marginTop: 20}}
              onChangeText={text => setFormData({ ...formData, name: text })}
            />

            <TextInput
              label="Sobrenome"
              value={formData.lastName}
              style={{marginTop: 20}}
              onChangeText={text => setFormData({ ...formData, lastName: text })}
            />
            
            <TextInput
              label="Identificador"
              value={formData.identifier}
              style={{marginTop: 20}}
              onChangeText={text => setFormData({ ...formData, identifier: text })}
            />

            <TextInput
              label="Tipo de Inscrição"
              value={formData.type}
              style={{marginTop: 20}}
              onChangeText={text => setFormData({ ...formData, type: text })}
            />
            
            <TextInput
              label="Valor pago"
              value={formData.cost}
              style={{marginTop: 20}}
              onChangeText={text => setFormData({ ...formData, cost: text })}
            />

            <TextInput
              label="Observações"
              value={formData.obs}
              style={{marginTop: 20}}
              onChangeText={text => setFormData({ ...formData, obs: text })}
            />

            
            <TextInput
              label="Permissões"
              value={formData.role}
              style={{marginTop: 20}}
              onChangeText={text => setFormData({ ...formData, role: text })}
            />
            
            <TextInput
              label="Histórico"
              value={formData.history}
              style={{marginTop: 20}}
              onChangeText={text => setFormData({ ...formData, history: text })}
            />


            <Button style={{marginTop: 20}} icon="calendar-search" mode="contained" onPress={() => createElement()}>
              Adicionar
            </Button>
          </View>
        </ScrollView>
      </>
    );
  }

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      padding: 20
    },
  
    fab: {
      position: 'absolute',
      margin: 16,
      right: 0,
      bottom: 0,
    },
  });
  
  export default SubscriberCreateScreen;