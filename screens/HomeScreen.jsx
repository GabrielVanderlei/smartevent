import { useIsFocused } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';

import { Appbar, Card, Paragraph, ProgressBar, Title } from 'react-native-paper';
import { FAB } from 'react-native-paper';

function HomeScreen({ navigation }) {
    const [events, setEvents] = useState({ data: [], loading: true });
    const isFocused = useIsFocused();

    let readEvents = async () => {
        let allEvents = await fetch("https://h8ojzpcs01.execute-api.us-east-2.amazonaws.com/events", {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        }).then((data) => data.json());

        setEvents(allEvents);
    }

    useEffect(() => {
        setEvents({ ...events, loading: true })
        readEvents();
    }, [isFocused])

    return (
        <>
            <ScrollView style={styles.container}>
                {events.loading ? (
                <ProgressBar indeterminate={true} color={"#800"} />
                ) : (
                <>
                    {events.data && events.data.length == 0 ? (
                    <Text style={{
                        padding: 40,
                        fontSize: 20
                    }}>
                        Adicione ou entre em um evento clicando no botão localizado no canto inferior direito da tela.
                    </Text>
                    ) : (
                    <>
                        {events.data.map((eventElement, index) => (
                            <TouchableOpacity 
                                style={styles.item} 
                                key={index} 
                                onPress={() => navigation.navigate('EventViewScreen', eventElement)}>
                                <Title>{eventElement.name}</Title>
                                <Paragraph>{eventElement.description}</Paragraph>
                            </TouchableOpacity>
                        ))}
                    </>
                    )}
                </>
                )}
            </ScrollView>
            <FAB
                style={styles.fab}
                big
                icon="plus"
                onPress={() => navigation.navigate('EventLocateScreen')}
            />
        </>
    );
}
  

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'white',
    },

    item: {
      backgroundColor: 'white',
      paddingHorizontal: 15, 
      paddingVertical: 9
    },
  
    fab: {
      position: 'absolute',
      margin: 16,
      right: 0,
      bottom: 0,
    },
});
  

  export default HomeScreen;