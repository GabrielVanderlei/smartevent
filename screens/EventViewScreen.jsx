import { useIsFocused } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

import { Appbar, Card, Paragraph, ProgressBar, Title } from 'react-native-paper';
import { FAB } from 'react-native-paper';

function EventViewScreen({ route, navigation }) {
    const [events, setEvents] = useState({ data: [], loading: true });
    const { sk } = route.params;
    const isFocused = useIsFocused();

    let readEvents = async () => {
        let allEvents = await fetch(`https://h8ojzpcs01.execute-api.us-east-2.amazonaws.com/events/personal?sk=${sk}`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        }).then((data) => data.json());

        if(allEvents.data && allEvents.data.length > 0){
            setEvents(allEvents);
        } else {
            setEvents({ data: [], loading: false})
        }
    }

    useEffect(() => {
        setEvents({ ...events, loading: true })
        readEvents();
    }, [isFocused])

    return (
        <>
            <View style={styles.container}>
                {events.loading ? (
                <ProgressBar indeterminate={true} color={"#800"} />
                ) : (
                <>
                    {events.data.length <= 0 ? (
                    <Text style={{
                        padding: 40,
                        fontSize: 20
                    }}>
                        Evento inexistente ou inacessível: {sk}
                    </Text>
                    ) : (
                    <>
                        <View style={{
                            padding: 20,
                            borderBottomWidth: 1,
                            borderBottomColor: 'rgb(230, 230, 230)'
                        }}>
                            <Title>{events.data[0].name}</Title>
                            <Paragraph>{events.data[0].description}</Paragraph>
                        </View>
                        <TouchableOpacity 
                            style={styles.item} 
                            onPress={() => navigation.navigate('SubscriberViewScreen', { sk, reload: true })}>
                            <Title>Inscritos</Title>
                        </TouchableOpacity>
                        <View style={{
                            padding: 20,
                            borderTopWidth: 1,
                            borderTopColor: 'rgb(230, 230, 230)',
                            marginTop: 50
                        }}>
                            <Title>Outras informações</Title>
                            <Paragraph>Identificador do evento: {events.data[0].sk}</Paragraph>
                            <Paragraph>Localização: {events.data[0].location}</Paragraph>
                            <Paragraph>Informações adicionais: {events.data[0].obs}</Paragraph>
                            <Paragraph>Custo de inscrição: {events.data[0].cost}</Paragraph>
                            <Paragraph>Contato: {events.data[0].contact}</Paragraph>
                        </View>
                    </>
                    )}
                </>
                )}
            </View>
        </>
    );
}
  

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'white',
    },

    item: {
      backgroundColor: 'white',
      paddingHorizontal: 15, 
      paddingVertical: 9
    },
  
    fab: {
      position: 'absolute',
      margin: 16,
      right: 0,
      bottom: 0,
    },
});
  

  export default EventViewScreen;