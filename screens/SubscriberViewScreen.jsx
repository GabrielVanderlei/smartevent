import { useIsFocused } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';

import { Appbar, Card, Paragraph, ProgressBar, Title } from 'react-native-paper';
import { FAB } from 'react-native-paper';

function SubscriberViewScreen({ route, navigation }) {
    const [subscribers, setSubscribers] = useState({ data: [], loading: true });
    const { sk } = route.params;
    const isFocused = useIsFocused();

    let readEvents = async () => {
        let allElements = await fetch(`https://h8ojzpcs01.execute-api.us-east-2.amazonaws.com/subscriber?sk=${sk}`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        }).then((data) => data.json());

        if(allElements.data && allElements.data.length > 0){
            setSubscribers(allElements);
        } else {
            setSubscribers({ data: [], loading: false})
        }
    }

    useEffect(() => {
        setSubscribers({ ...subscribers, loading: true });
        readEvents();
    }, [isFocused])

    return (
        <>
            <ScrollView>
                <View style={styles.container}>
                    {subscribers.loading ? (
                    <ProgressBar indeterminate={true} color={"#800"} />
                    ) : (
                    <>
                        <TouchableOpacity style={styles.item} onPress={() => navigation.navigate('SubscriberCreateScreen', { sk }) }>
                            <Title>Adicionar inscrito</Title>
                        </TouchableOpacity>
                        {subscribers.data.length <= 0 ? (
                        <Text style={{
                            padding: 40,
                            fontSize: 20
                        }}>
                            Nenhum inscrito localizado
                        </Text>
                        ) : (
                        <>
                            {subscribers.data.map((element) => (
                                <TouchableOpacity style={styles.item}>
                                    <Title>{element.name || '-'} {element.lastName || '-'}</Title>
                                    <Paragraph>Nome do evento: {element.eventName || "Não definido"}</Paragraph>
                                    <Paragraph>Identificador: {element.identifier || "Não definido"}</Paragraph>
                                    <Paragraph>Tipo; {element.type || "Não definido"}</Paragraph>
                                    <Paragraph>Custo: {element.cost || "Não definido"}</Paragraph>
                                    <Paragraph>Observações: {element.obs || "Não definido"}</Paragraph>
                                    <Paragraph>Permissão: {element.role || "Não definido"}</Paragraph>
                                    <Paragraph>Histórico: {element.history || "Não definido"}</Paragraph>
                                </TouchableOpacity>
                            ))}
                        </>
                        )}
                    </>
                    )}
                </View>
            </ScrollView>
        </>
    );
}
  

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'white',
    },

    item: {
      backgroundColor: 'white',
      paddingHorizontal: 15, 
      paddingVertical: 9
    },
  
    fab: {
      position: 'absolute',
      margin: 16,
      right: 0,
      bottom: 0,
    },
});
  

  export default SubscriberViewScreen;