import React, { useEffect, useState } from 'react';
import { ScrollView, StyleSheet, Text, View } from 'react-native';

import { TextInput } from 'react-native-paper';
import { Button } from 'react-native-paper';

function EventCreateScreen({ route, navigation }) {
    const [formData, setFormData] = React.useState({
        name: '',
        description: '',
        location: '',
        obs: '',
        cost: '',
        contact: ''
    });

    const createElement = async () => {
        let allElements = await fetch(`https://h8ojzpcs01.execute-api.us-east-2.amazonaws.com/events`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(formData)
        }).then((data) => data.json());

        if(allElements.data){
            alert(JSON.stringify(allElements));
            navigation.navigate('HomeScreen')
        } else {
            alert(JSON.stringify(allElements));
            alert("Erro ao adicionar evento")
        }
    }
  
    return (
      <>
        <ScrollView>
          <View  style={styles.container}>
            <TextInput
              label="Nome"
              value={formData.name}
              style={{marginTop: 20}}
              onChangeText={text => setFormData({ ...formData, name: text })}
            />

            <TextInput
              label="Descrição"
              value={formData.description}
              style={{marginTop: 20}}
              onChangeText={text => setFormData({ ...formData, description: text })}
            />
            
            <TextInput
              label="Local"
              value={formData.location}
              style={{marginTop: 20}}
              onChangeText={text => setFormData({ ...formData, location: text })}
            />

            <TextInput
              label="Informações adicionais"
              value={formData.obs}
              style={{marginTop: 20}}
              onChangeText={text => setFormData({ ...formData, obs: text })}
            />
            
            <TextInput
              label="Custo da inscrição"
              value={formData.cost}
              style={{marginTop: 20}}
              onChangeText={text => setFormData({ ...formData, cost: text })}
            />
            
            <TextInput
              label="Forma de contato"
              value={formData.contact}
              style={{marginTop: 20}}
              onChangeText={text => setFormData({ ...formData, contact: text })}
            />

            <Button style={{marginTop: 20}} icon="calendar-search" mode="contained" onPress={() => createElement()}>
              Adicionar
            </Button>
          </View>
        </ScrollView>
      </>
    );
  }

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      padding: 20
    },
  
    fab: {
      position: 'absolute',
      margin: 16,
      right: 0,
      bottom: 0,
    },
  });
  
  export default EventCreateScreen;